/**
 * @author Dicky Febrian Dwiputra (3411181097) Kelas D
 */
package Pertemuan3;

public class P1FactorialIter {
	static int factorial(int n) {
		int result=1;
		for (int i=n; i>1; i--) {
			result *= i;
		}
		return result;
	}
	
	public static void main(String args[]) {
		int n = Integer.parseInt(args[0]);
		System.out.println(factorial(n));
	}
}
//Menambahkan comment
