/**
 * @author Dicky Febrian Dwiputra (3411181097) Kelas D
 */
package Pertemuan3;

public class P2FactorialIter {
	static int factorial(int n) {
		if(n==0 || n==1) return 1;
		return factorial(n-1)*n;
	}
	
	public static void main(String args[]) {
		int n = Integer.parseInt(args[0]);
		System.out.println(factorial(n));
	}
}
